<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['favicon']; ?>
    <?php print $print['css']; ?>
  </head>
  <body>
    <?php $campo = $_GET["field"]; 	?>

    <?php if (!empty($print['message'])) {
		      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>
    <div class="print-logo"><?php print $print['logo']; ?></div>
    <p />


    <h1><?php print $print['title']; ?></h1>

    <div class="print-content"><?php //print $print['content']; ?></div>


	<!-- MOSTRAMOS DISTINTOS BLOCKS EN LA P�GINA DE IMPRESI�N -->

	<div class="print-blocks">
		<?php 	
/*				// MAPA
				$block = module_invoke('cck_blocks', 'block_view', 'field_mapa');
				print render($block['content']);

				// ALTIMETR�A FOTO
				$block = module_invoke('cck_blocks', 'block_view', 'field_altimetria_foto');
				print render($block['content']);

				// ALTIMETR�A TEXTO
				$block = module_invoke('cck_blocks', 'block_view', 'field_altimetria_texto');
				print render($block['content']);

				// EL TIEMPO
				$block = module_invoke('cck_blocks', 'block_view', 'field_el_tiempo');
				print render($block['content']);

				// COMENTARIOS
				$block = module_invoke('commentsblock', 'block_view', 'comment_form_block');
				print render($block['content']);		*/


				// DEPENDIENDO DEL CAMPO "FIELD" QUE NOS LLEGA DE LA "VIEW" RESPECTIVA, MOSTRAMOS UN CAMPO U OTRO
				switch ($campo) {
					case "datos":	$bloque = 'field_datos_de_inter_s';
									$titulo = t('Interesting facts');
									break;
					case "pistas":	$bloque = 'field_pistas_ignacianas';
									$titulo = t('Ignatian tips');
									break;
					case "auto":	$bloque = 'field_autobiografia';
									$titulo = t('Autobiography');
									break;
					default:		$bloque = '';
									break;
				}
				
				print '<h2>' . $titulo . '</h2>';

				// MOSTRAMOS EL CAMPO SELECCIONADO
				$block = module_invoke('cck_blocks', 'block_view', $bloque);
				print render($block['content']);

		 ?>
	</div>

    <div class="print-footer"><?php print $print['footer_message']; ?></div>
    <div class="print-source_url"><?php //print $print['source_url']; ?></div>
    <div class="print-links"><?php print $print['pfp_links']; ?></div>
    <?php print $print['footer_scripts']; ?>
  </body>
</html>
