<?php

/**
 * @file
 * Default views template for displaying a slideshow.
 *
 * - $view: The View object.
 * - $options: Settings for the active style.
 * - $rows: The rows output from the View.
 * - $title: The title of this group of rows. May be empty.
 *
 * @ingroup views_templates
 */
?>

<div class="skin-<?php print $skin; ?>">
  <?php if (!empty($top_widget_rendered)): ?>
    <div class="views-slideshow-controls-top clearfix">
      <?php print $top_widget_rendered; ?>
    </div>
  <?php endif; ?>

<?php 

	// VIEW CAROUSEL ETAPAS - MOSTRAMOS DESTACADA LA ETAPA ACTUAL

	// DEVEL:			dpm($view);			dpr($slideshow);

	// CON JQUERY, AVERIGUAMOS EL 'PARENT' DE LA ETAPA ACTUAL, Y LE ASIGNAMOS LA CLASE 'ACTIVE', PARA MOSTRARLA RESALTADA CON CSS

	drupal_add_js("	
		jQuery(document).ready(function () { 

			jQuery('div.view-etapas .views-row a.active').parents('.views-row').addClass('active');
			
		}

	);", "inline");	
?>

  <?php print $slideshow; ?>


  <?php if (!empty($bottom_widget_rendered)): ?>
    <div class="views-slideshow-controls-bottom clearfix">
      <?php print $bottom_widget_rendered; ?>
    </div>
  <?php endif; ?>
</div>
