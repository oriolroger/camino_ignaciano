// JavaScript Document

jQuery(document).ready(function($){

	// FIXED MENU HEADER
	var navpos = $('#nav-bar').offset();
	$(window).bind('scroll', function() {
		if ($(window).scrollTop() > navpos.top) {
			$('#nav-bar').addClass('fixed-nav');
		}
		else {
			$('#nav-bar').removeClass('fixed-nav');
		}
	});
	// FIXED MENU HEADER


	// CAMINO DEL RESPETO - MOVER TÍTULO ANTES DEL TEXTO DE INTRODUCCIÓN
	if ($('body.page-oficina-peregrino-camino-del-respeto').length) {
//		console.log('DENTRO');
		introduccion = $('#content .block.first');
		vista = $('#content .view-oficina-de-acogida-al-peregrino .view-header');
		introduccion.insertAfter(vista);
	}
	// CAMINO DEL RESPETO - MOVER TÍTULO ANTES DEL TEXTO DE INTRODUCCIÓN




})
