<?php
/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can modify or override Drupal's theme
 *   functions, intercept or make additional variables available to your theme,
 *   and create custom PHP logic. For more information, please visit the Theme
 *   Developer's Guide on Drupal.org: http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to STARTERKIT_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: STARTERKIT_breadcrumb()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override either of the two theme functions used in Zen
 *   core, you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function ignaciano_preprocess_page(&$variables, $hook) {

	// SI TIENE EL PAR�METRO "AJAX", LO MOSTRAMOS CON LA VENTANA DEL "COLORBOX"
	if ( isset($_GET['colorbox']) && $_GET['colorbox'] == 1 ) {
		$variables['theme_hook_suggestions'][] = 'page__colorbox';
  	}
  
  
  
	// SI UNA P�GINA NO TIENE SUBMEN�S, ESCONDEMOS LA REGI�N "SUBMENU"
	drupal_add_js("	
		jQuery(document).ready(function () { 
			field = jQuery('div#submenu');
			str = field.text();

			if(jQuery.trim(str) === '') {
				field.remove();
			}
		}
	);", "inline");



	// ESCONDEMOS EL BOT�N "PRINT PDF" EN LA P�GINA INICIAL
	drupal_add_js("	
		jQuery(document).ready(function () { 
			field = jQuery('ul li.printfriendly');

			if (jQuery('body').hasClass('front')) {
				field.remove();
			}
		}
	);", "inline");







}


/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */

function ignaciano_preprocess_node(&$variables, $hook) {

	$node = $variables['node'];

	// ESCRIBIMOS EL NID DEL NODE ORIGINAL, EN EL CASO QUE SEA UNA TRADUCCI�N
/*	$translations = translation_node_get_translations($node->tnid);

	foreach($translations as $value) {
		echo "NID:".$value->language;
	}

	// CONSEGUIMOS UN ARRAY SOLO CON LOS IDIOMAS, Y MIRAMOS SI EL IDIOMA ACTUAL EST� DENTRO DEL ARRAY
	global $language;
	$lang = $language->language;
	
	$idiomas = array_keys($translations);

	if (in_array($lang,$idiomas))
		echo "NID:".$lang;
	}
*/




	// SI ESTAMOS EN UNA P�GINA DE TIPO ETAPA
	if ($node->type == "etapa") {

		// SI SE HA ENTRADO INTRODUCCI�N, ESCONDEMOS EL CAMPO BODY, Y LO MOSTRAMOS M�S TARDE, SI HACEN CLICK EN "LEER M�S"
		if (!empty($node->field_introduccio)) {

			drupal_add_js("	
				jQuery(document).ready(function () { 

					// MOSTRAMOS EL ENLACE 'LEER M�S'
					html = '<div><a href=\'#\' class=\'read_more\' id=\'read_more_link\'>" . t('Read more') . "</a></div>';
					jQuery('div.field-name-body').after(html);

					// ESCONDEMOS EL CAMPO BODY
					jQuery('div.field-name-body').hide();


					jQuery('a.read_more').click(function() {  
						jQuery('div.field-name-body').slideToggle(800,function(){
						});
					});

				}
			);", "inline");

		}


	}


	// JQUERY PARA MOSTRAR O NO MOSTRAR CAMPOS CUANDO IMPRIMIMOS O EN PDF

	drupal_add_js("	
		jQuery(document).ready(function () { 
			jQuery('div#main').addClass('print-only');											// MOSTRAMOS TODO

			jQuery('ul.flippy').addClass('print-no');											// ESCONDEMOS LINKS A ETAPA ANTERIOR Y SIGUIENTE

			jQuery('div.block-cck-blocks').addClass('print-no');								// ESCONDEMOS CAMPOS DE BLOQUES (CCK BLOCKS)
			jQuery('div#block-views-etapas-block').addClass('print-no');						// ESCONDEMOS FOTOS DE OTRAS ETAPAS
			jQuery('div.view-banners-alojamiento-comunidad').addClass('print-no');				// ESCONDEMOS BANNERS
			jQuery('div.view-banners-alojamiento-etapa').addClass('print-no');					// ESCONDEMOS BANNERS
			jQuery('div#block-treewalk-treewalk-menu-prev-next').addClass('print-no');			// BOTON PREVIOUS - NEXT

			jQuery('div#block-views-gps-track-block').addClass('print-no');						// GPS TRACK
			jQuery('div#block-block-5').addClass('print-no');									// ESCONDEMOS COMENTARIOS
			jQuery('div.block-commentsblock').addClass('print-no');								// ESCONDEMOS COMENTARIOS
			jQuery('div#block-views-datos-de-interes-block').addClass('print-no');				// DATOS DE INTERES
			jQuery('div#block-views-pistas-ignacianas-block').addClass('print-no');				// PISTAS IGNACIANAS
			jQuery('div#block-views-autobiografia-block').addClass('print-no');					// AUTOBIOGRAFIA

		}
	);", "inline");	




	// EN LA P�GINA DE COMENTARIOS (COLORBOX), MOSTRAMOS EL LOGO SUPERIOR
	drupal_add_js("	
		jQuery(document).ready(function () {
			field = jQuery('.block-commentsblock');
			field.prepend('<span><img src=\"/sites/default/files/logo-web-petit.png\"></span>');
		}
	);", "inline");






			// JQUERY PARA ESCONDER EL ENLACE A LA ETAPA ANTERIOR O SIGUIENTE
/*			drupal_add_js("	
				jQuery(document).ready(function () { 
					var act = window.location.pathname;
					var ant = jQuery('ul.flippy li.previous a').attr('href');
					var sig = jQuery('ul.flippy li.next a').attr('href');
		
					var actBase = act.substring(0,act.lastIndexOf('/'));
					
					if (ant) {
						var antBase = ant.substring(0,ant.lastIndexOf('/'));
						if (actBase != antBase)	{
							jQuery('ul.flippy li.previous a').hide();
						}
					}

					if (sig) {
						var sigBase = sig.substring(0,sig.lastIndexOf('/'));
		
						if (actBase != sigBase)	{
							jQuery('ul.flippy li.next a').hide();
						}
					}
		
				}
			);", "inline");			*/

}


/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */

function ignaciano_preprocess_comment(&$variables, $hook) {
}


/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  $variables['classes_array'][] = 'count-' . $variables['block_id'];
}
// */





// MODIFICAMOS LAS OPCIONES DEL MEN� SUPERFISH - SOLO QUEREMOS QUE SE MUESTREN LOS MEN�S DESPLEGABLES EN LAS ETAPAS
function ignaciano_superfish_build($variables) {
  $output = array('content' => '');
  $id = $variables['id'];
  $menu = $variables['menu'];
  $depth = $variables['depth'];
  $trail = $variables['trail'];
  // Keep $sfsettings untouched as we need to pass it to the child menus.
  $settings = $sfsettings = $variables['sfsettings'];
  $megamenu = $settings['megamenu'];
  $total_children = $parent_children = $single_children = 0;
  $i = 1;
  
//  echo "ID:".$id."<br>";
//  print_r($menu);

  // Reckon the total number of available menu items.
  foreach ($menu as $menu_item) {
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $total_children++;
    }
  }

  foreach ($menu as $menu_item) {

    $show_children = $megamenu_wrapper = $megamenu_column = $megamenu_content = FALSE;
    $item_class = $link_options = $link_class = array();
    $mlid = $menu_item['link']['mlid'];
  
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $item_class[] = ($trail && in_array($mlid, $trail)) ? 'active-trail' : '';

      // Add helper classes to the menu items and hyperlinks.
      $settings['firstlast'] = ($settings['dfirstlast'] == 1 && $total_children == 1) ? 0 : $settings['firstlast'];
      $item_class[] = ($settings['firstlast'] == 1) ? (($i == 1) ? 'first' : (($i == $total_children) ? 'last' : 'middle')) : '';
      $settings['zebra'] = ($settings['dzebra'] == 1 && $total_children == 1) ? 0 : $settings['zebra'];
      $item_class[] = ($settings['zebra'] == 1) ? (($i % 2) ? 'odd' : 'even') : '';
      $item_class[] = ($settings['itemcount'] == 1) ? 'sf-item-' . $i : '';
      $item_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $link_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $item_class[] = ($settings['liclass']) ? $settings['liclass'] : '';
      if (strpos($settings['hlclass'], ' ')) {
        $l = explode(' ', $settings['hlclass']);
        foreach ($l as $c) {
          $link_class[] = $c;
        }
      }
      else {
        $link_class[] = $settings['hlclass'];
      }
      $i++;

      // Add hyperlinks description (title) to their text.
      $show_linkdescription = ($settings['linkdescription'] == 1 && !empty($menu_item['link']['localized_options']['attributes']['title'])) ? TRUE : FALSE;
      if ($show_linkdescription) {
        if (!empty($settings['hldmenus'])) {
          $show_linkdescription = (is_array($settings['hldmenus'])) ? ((in_array($mlid, $settings['hldmenus'])) ? TRUE : FALSE) : (($mlid == $settings['hldmenus']) ? TRUE : FALSE);
        }
        if (!empty($settings['hldexclude'])) {
          $show_linkdescription = (is_array($settings['hldexclude'])) ? ((in_array($mlid, $settings['hldexclude'])) ? FALSE : $show_linkdescription) : (($settings['hldexclude'] == $mlid) ? FALSE : $show_linkdescription);
        }
        if ($show_linkdescription) {
          $menu_item['link']['title'] .= '<span class="sf-description">';
          $menu_item['link']['title'] .= (!empty($menu_item['link']['localized_options']['attributes']['title'])) ? $menu_item['link']['localized_options']['attributes']['title'] : array();
          $menu_item['link']['title'] .= '</span>';
          $link_options['html'] = TRUE;
        }
      }

      // Add custom HTML codes around the menu items.
      if ($sfsettings['wrapul'] && strpos($sfsettings['wrapul'], ',') !== FALSE) {
        $wul = explode(',', $sfsettings['wrapul']);
        // In case you just wanted to add something after the element.
        if (drupal_substr($sfsettings['wrapul'], 0) == ',') {
          array_unshift($wul, '');
        }
      }
      else {
        $wul = array();
      }

      // Add custom HTML codes around the hyperlinks.
      if ($settings['wraphl'] && strpos($settings['wraphl'], ',') !== FALSE) {
        $whl = explode(',', $settings['wraphl']);
        // The same as above
        if (drupal_substr($settings['wraphl'], 0) == ',') {
          array_unshift($whl, '');
        }
      }
      else {
        $whl = array();
      }

      // Add custom HTML codes around the hyperlinks text.
      if ($settings['wraphlt'] && strpos($settings['wraphlt'], ',') !== FALSE) {
        $whlt = explode(',', $settings['wraphlt']);
        // The same as above
        if (drupal_substr($settings['wraphlt'], 0) == ',') {
          array_unshift($whlt, '');
        }
        $menu_item['link']['title'] = $whlt[0] . check_plain($menu_item['link']['title']) . $whlt[1];
        $link_options['html'] = TRUE;
      }


	  // IGNACIANO - ORIOL ROGER - DESACTIVAMOS DESPLEGABLES DE LAS OPCIONES DE MEN� QUE NO S�N ETAPAS
/*	  $menu_no_children = array(
			"435",	// HISTORIA DEL CAMINO
			"466",	// CONSEJOS PR�CTICOS
			"481"	// PEREGRINACI�N
	  );		*/
	  
	  // ASIGNAMOS EL NODE->NID DE LA TRADUCCI�N "ORIGINAL"
	  $menu_no_children = array(
			"1",	// HISTORIA DEL CAMINO
			"18",	// CONSEJOS PR�CTICOS
			"47"	// PEREGRINACI�N
	  );

	  // BUSCAMOS EL NODE->NID DE LA OPCI�N DE MEN� ACTUAL, Y DE ALL� SACAMOS EL NODE->TID, QUE ES EL NID DE LA TRADUCCI�N "ORIGINAL"
	  // SI EST� DENTRO DEL ARRAY DE MENU_NO_CHILDREN, NO MOSTRAMOS EL DESPLEGABLE
	  $numero = $menu_item['link']['link_path'];
	  $text = "node/";
		
	  if (substr($numero, 0, strlen($text)) == $text) {
		$numero = substr($numero, strlen($text));
	  }
	  $node = node_load($numero);


//    	if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0) {
//      if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0 && !in_array($mlid, $menu_no_children)) {
      if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0 && !in_array($node->tnid, $menu_no_children)) {

        // Megamenu is still beta, there is a good chance much of this will be changed.
        if (!empty($settings['megamenu_exclude'])) {
          if (is_array($settings['megamenu_exclude'])) {
            $megamenu = (in_array($mlid, $settings['megamenu_exclude'])) ? 0 : $megamenu;
          }
          else {
            $megamenu = ($settings['megamenu_exclude'] == $mlid) ? 0 : $megamenu;
          }
          // Send the result to the sub-menu.
          $sfsettings['megamenu'] = $megamenu;
        }
        if ($megamenu == 1) {
          $megamenu_wrapper = ($menu_item['link']['depth'] == $settings['megamenu_depth']) ? TRUE : FALSE;
          $megamenu_column = ($menu_item['link']['depth'] == $settings['megamenu_depth'] + 1) ? TRUE : FALSE;
          $megamenu_content = ($menu_item['link']['depth'] >= $settings['megamenu_depth'] && $menu_item['link']['depth'] <= $settings['megamenu_levels']) ? TRUE : FALSE;
        }
        // Render the sub-menu.
        $var = array(
          'id' => $id,
          'menu' => $menu_item['below'],
          'depth' => $depth, 'trail' => $trail,
          'sfsettings' => $sfsettings
        );
        $children = theme('superfish_build', $var);
        // Check to see whether it should be displayed.
        $show_children = (($menu_item['link']['depth'] <= $depth || $depth == -1) && $children['content']) ? TRUE : FALSE;
        if ($show_children) {
          // Add item counter classes.
          if ($settings['itemcounter']) {
            $item_class[] = 'sf-total-children-' . $children['total_children'];
            $item_class[] = 'sf-parent-children-' . $children['parent_children'];
            $item_class[] = 'sf-single-children-' . $children['single_children'];
          }
          // More helper classes.
          $item_class[] = ($megamenu_column) ? 'sf-megamenu-column' : '';
          $item_class[] = $link_class[] = 'menuparent';
        }
        $parent_children++;
      }
      else {
        $item_class[] = 'sf-no-children';
        $single_children++;
      }

      $item_class = implode(' ', array_filter($item_class));
      
      if (isset($menu_item['link']['localized_options']['attributes']['class'])) {
        $link_class_current = $menu_item['link']['localized_options']['attributes']['class'];
        $link_class = array_merge($link_class_current, array_filter($link_class));
      }
      $menu_item['link']['localized_options']['attributes']['class'] = $link_class;
 
      $link_options['attributes'] = $menu_item['link']['localized_options']['attributes'];
      
      // Render the menu item.
      $output['content'] .= '<li id="menu-' . $mlid . '-' . $id . '"';
      $output['content'] .= ($item_class) ? ' class="' . trim($item_class) . '">' : '>';
      $output['content'] .= ($megamenu_column) ? '<div class="sf-megamenu-column">' : '';
      $output['content'] .= isset($whl[0]) ? $whl[0] : '';
      $output['content'] .= l($menu_item['link']['title'], $menu_item['link']['link_path'], $link_options);
      $output['content'] .= isset($whl[1]) ? $whl[1] : '';
      $output['content'] .= ($megamenu_wrapper) ? '<ul class="sf-megamenu"><li class="sf-megamenu-wrapper ' . $item_class . '">' : '';
      $output['content'] .= ($show_children) ? (isset($wul[0]) ? $wul[0] : '') : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '<ol>' : '<ul>') : '';
      $output['content'] .= ($show_children) ? $children['content'] : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '</ol>' : '</ul>') : '';
      $output['content'] .= ($show_children) ? (isset($wul[1]) ? $wul[1] : '') : '';
      $output['content'] .= ($megamenu_wrapper) ? '</li></ul>' : '';
      $output['content'] .= ($megamenu_column) ? '</div>' : '';
      $output['content'] .= '</li>';
    }
  }
  $output['total_children'] = $total_children;
  $output['parent_children'] = $parent_children;
  $output['single_children'] = $single_children;
  return $output;
}





// CAMBIAMOS LOS FILTROS DE LA VIEW DE ALOJAMIENTO
function ignaciano_preprocess_views_view(&$vars) {
  $function_name = 'ignaciano_preprocess_views_view__' . $vars['view']->name;
  if (function_exists($function_name)) {
    $function_name($vars);
  }
}

function ignaciano_preprocess_views_view__alojamiento_global(&$vars) {
/*
	  $view = $vars['view'];
//	  dpm($vars['view']->filter['php']['options']);

		dpm($view);

		$display_id = 'block_1';

		$options = array(
		  'operator' => 'in',
		  'value' => array(
			'ctype1' => 'ctype1',
			'ctype2' => 'ctype2',
		  ),
		  'group' => '0',
		  'exposed' => FALSE,
		  'expose' => array(
			'operator' => FALSE,
			'label' => '',
		  ),
		  'id' => 'type',
		  'relationship' => 'none',
		);

		$view->add_item($display_id, 'filter', 'node', 'type', $options);

		dpm($view);
*/
/*
	  $rows = $vars['rows'];

	  //Sort rows into letter sets
	  $letters = array();
	  $array_actual = new stdClass;
	  $objeto = new stdClass;

	  $i = 0;
	  $actual = 3;
	  $push = true;

	  dpm($vars);

	  foreach ($results as $result) {
		$first_letter = $result->field_data_field_numero_de_etapa_field_numero_de_etapa_value;

//		if (is_array($letters[$first_letter])) {
//		  array_push($letters[$first_letter], $result);
//		}
//		else {
//		  $letters[$first_letter] = array($result);
//		}
		$objeto = $result;
//		array_push($letters, $objeto);

		if ($first_letter == $actual) {
			$push = false;
			$array_actual = $objeto;
		}
		else {
			if ($push) {
				array_push($letters, $objeto);
			}
			else {
				array_unshift($letters, $objeto);
			}
		}	
//dpm($letters);

		$i++;
	  }

	  array_unshift($letters, $array_actual);

//	dpm($letters);


	  //Add to variables
	  $vars['letters'] = $letters;
	  $vars['results'] = $letters;
*/
}



/*
function ignaciano_views_filters($form) {
	print_r($form);

//  if ($form['#view_name'] == 'view-clone-of-alojamiento-global') {
  if ($form['#view_name'] == 'view-alojamiento-global') {

  

    $view = $form['view']['#value'];
    $form['submit']['#value'] = 'Search';
    
    $rows_theme = '';
    
    foreach ($view->exposed_filter as $count => $expose) {
      
      $rows_theme .= '<div class="filter ' . $form["filter$count"]['#name'] . '">';
      $rows_theme .= '<label for="' . $form["filter$count"]['#id'] . '">'. $expose['label'] .'</label>';
      $rows_theme .= drupal_render($form["op$count"]) . drupal_render($form["filter$count"]);
      $rows_theme .= '</div>';
    }
      $rows_theme .= '<div>'. drupal_render($form['submit']) .'</div>';

    
    return drupal_render($form['q']) . $rows_theme . drupal_render($form);
  }
  else {
    return theme_views_filters($form);
  }
  
}
*/










// CAMBIAMOS LA VIEW DE ETAPAS

/*
function ignaciano_preprocess_views_view(&$vars) {
  $function_name = 'ignaciano_preprocess_views_view__' . $vars['view']->name;
  if (function_exists($function_name)) {
    $function_name($vars);
  }
}



//Change mytheme to your theme name

function ignaciano_preprocess_views_view__etapas(&$vars) {

	  $results = $vars['view']->result;
//	  dpm($results);

	  $rows = $vars['rows'];



	  //Sort rows into letter sets
	  $letters = array();
	  $array_actual = new stdClass;
	  $objeto = new stdClass;

	  $i = 0;
	  $actual = 3;
	  $push = true;

	  dpm($vars);

	  foreach ($results as $result) {
		$first_letter = $result->field_data_field_numero_de_etapa_field_numero_de_etapa_value;

//		if (is_array($letters[$first_letter])) {
//		  array_push($letters[$first_letter], $result);
//		}
//		else {
//		  $letters[$first_letter] = array($result);
//		}
		$objeto = $result;
//		array_push($letters, $objeto);

		if ($first_letter == $actual) {
			$push = false;
			$array_actual = $objeto;
		}
		else {
			if ($push) {
				array_push($letters, $objeto);
			}
			else {
				array_unshift($letters, $objeto);
			}
		}	
//dpm($letters);

		$i++;
	  }

	  array_unshift($letters, $array_actual);

//	dpm($letters);


	  //Add to variables
	  $vars['letters'] = $letters;
	  $vars['results'] = $letters;

}
*/



// TEMPORALMENTE, ESCONDEMOS LOS ICONOS DE LOS IDIOMAS QUE NO TENEMOS TRADUCIDOS
function ignaciano_links__locale_block($variables) {
  global $language;
  $idiomas = array('');
  foreach ($idiomas as $value) {
	unset($variables['links'][$value]);
  }

  return theme('links', $variables);
}