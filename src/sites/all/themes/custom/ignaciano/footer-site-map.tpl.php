<?php
/**
 * @file
 * The whole footer-sitemap container.
 */
?>
<div id="footer-sitemap" class="clearfix">
  <div class="fs-block-content">
  	<?php 
//			dpm($site_map); 
			print render($site_map); 
	?>
  </div>
  <div class="footer-contact-link">
  		<?php 
			$site_email = variable_get('site_mail', '');
			print(l($site_email,"mailto:".$site_email));
		?>
  </div>
</div>
